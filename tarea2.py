import time
import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize
from matplotlib.cm import ScalarMappable
from IPython.display import clear_output


# Crear una nueva cuadrícula con dos especies de dimensiones x por y (ancho por alto)
def new_board(x, y):
    """Inicializa un tablero para el juego de la vida con dos especies."""
    board = []
    for i in range(y):
        row = []
        for j in range(x):
            row.append(random.choice([0, 1, 2]))  # escogemos aleatoriamente entre {0,1,2}. Donde, por enunciado, 0 es muerta, 1 es especie A, 2 es especie B
        board.append(row)                         # contruimos fila a fila, tantas como la altura lo señale
    return board

# Obtener el estado de una celda: considerando los límites del tablero y, por asi decirlo, condiciones periodicas. Piense que si el tablero es bxb con
# b un numero entero. Para otro numero entero z, se cumple que z = b*c + r (de manera que z%b = r). De manera que si por ejm, b=10, se sigue que
# z/x = z/10 i,e si z = 2 obtendremos que z = b*c + r = 10*0 + 2 de manera que z/x = 0.2 ...... ahora bien, si z=15 se sigue que z = 10*1 + 5 de manera que
# z/x = 1.5 ..... en general notemos que para z de 0 a 9 va dar 0.0, 0.1, 0.2, 0.3, ... 0.9. Para z de 10 a 19 da 1.0, 1.1, 1.2, 1.2, ... 1.9. Por lo que
# notamos que los decimales se repiten cada 10 (de 10 en 10). Si b=5, tambien se repetiria cada 5, pues note que z=2=5*0+2 y z=7=5*1+2 y z=12=5*2+2 y etc
# (se repite el residuo).
def get(board, x, y):
    """Obtiene el estado-valor asociado a una celda"""
    return board[y % len(board)][x % len(board[0])]

# Asignar un valor a una celda: saca-busca el valor de manera analoga como antes, y ademas le asigna un valor
def assign(board, x, y, value):
    """Cambia-asigna un valor a una celda"""
    board[y % len(board)][x % len(board[0])] = value

# Contar vecinos de una especie específica
def count_neighbors(board, x, y, species):
    """Cuenta los vecinos de una especie específica"""
    offsets = [(-1, -1), (-1, 0), (-1, 1),     #esto nos permitira revizar los vecinos en un cuadrado entorno a un punto central
               (0, -1),           (0, 1),      #desde la esquina superior izquierda (-1,-1) hasta la esquina inferior derecha (1,1)
               (1, -1),  (1, 0),  (1, 1)]      #recorriendo asi desde la primera fila hasta la ultima
    count = 0
    for dx, dy in offsets:
        if get(board, x + dx, y + dy) == species:
            count = count + 1
    return count

# Procesar una iteración del juego
def process_life(board):
    """Realiza una iteración del juego de la vida (y devuelve el tablero despues de dicha iteracion 
    i,e los cambios que sufre el mismo)"""
    next_board = new_board(len(board[0]), len(board))
    for y in range(len(board)):
        for x in range(len(board[0])):
            cell = get(board, x, y)                        # extrae uno
            neighbors_A = count_neighbors(board, x, y, 1)  # mira cuales de los que le rodea son tipo A
            neighbors_B = count_neighbors(board, x, y, 2)  # mira cuales de los que le rodea son tipo B

            # Reglas de supervivencia
            if cell == 1 and 2 <= neighbors_A <= 3 and neighbors_B <= 2:
                assign(next_board, x, y, 1)
            elif cell == 2 and 2 <= neighbors_B <= 3 and neighbors_A <= 2:
                assign(next_board, x, y, 2)
            # Reglas de muerte
            elif cell == 1 and (neighbors_A > 3 or neighbors_A < 2 or neighbors_B > 2):
                assign(next_board, x, y, 0)
            elif cell == 2 and (neighbors_B > 3 or neighbors_B < 2 or neighbors_A > 2):
                assign(next_board, x, y, 0)
            # Reglas de nacimiento
            elif cell == 0 and (neighbors_A == 3 and neighbors_B <= 2):
                assign(next_board, x, y, 1)
            elif cell == 0 and (neighbors_B == 3 and neighbors_A <= 2):
                assign(next_board, x, y, 2)
            # Condición adicional de competencia: cazador-presa
            elif cell == 1 and neighbors_B >= 2:
                assign(next_board, x, y, 2)
            else:
                # Si no se cumple ninguna regla, la célula se mantiene o muere.... no es parte de las normas perse, pero esta en el colab de clase
                assign(next_board, x, y, 0)
    return next_board

# Para ver los resultados, imprimimos el tablero inicial y el resultado después de una iteración and so on
def draw_board(board):       # para dibujar el tablero
    """Dibuja el tablero que se pase por argumento"""
    res = ''
    for row in board:
        for col in row:
            if col == 1:
                res += '* '  # Especie A
            elif col == 2:
                res += '# '  # Especie B
            else:
                res += '. '  # Célula muerta
        res += '\n'
    return res

# Inicializamo un tablero (para poder llegar a cabo la interacción entre especies)
board = new_board(50, 50)

NUM_ITERATIONS = 400

for i in range(0, NUM_ITERATIONS):
    print('Iteration ' + str(i + 1))
    board = process_life(board)
    res = draw_board(board)
    print(res)
    time.sleep(0.1)  # Pausa de 0.1 segundos para que sea visible la evolución
    clear_output(wait=True)

    def count_cells(board):
        count_A = sum(row.count(1) for row in board)
        count_B = sum(row.count(2) for row in board)
        return count_A, count_B

# Ejecutemos el juego durante 1000 iteraciones:
board = new_board(50, 50)
NT = 50 * 50              #número total de espacios en el tablero NT
counts_A = []
counts_B = []
joint_distribution = {}   #pa contar las ocurrencias conjuntas al presuponer que "es probable xD" que la prob de A depende de B

for _ in range(1000):
    board = process_life(board)
    count_A, count_B = count_cells(board)       #count_A=NA y count_B=NB
    counts_A.append(count_A)
    counts_B.append(count_B)
    joint_distribution[(count_A, count_B)] = joint_distribution.get((count_A, count_B), 0) + 1   # frec de cada estado del sistema (NA,NB) i,e distrb conj

# De manera que la probabilidad conjunta estara dada al dividir por el número de iteraciones...
# Es decir, debemos de convirtir el conteo-frec en una frecuencia relativa o una probabilidad (recuerde, que por dfn, la probabilidad de un evento -o estado
# ennurestro caso- es el número de veces que ocurre ese evento dividido por el número total de oportunidades que tiene de ocurrir). En todo caso:
for state, count in joint_distribution.items():
    joint_distribution[state] = count / 1000.0       # ya ahora en vez de asociar cada estado en el diccionario a una frec, se le asociad un frec realtv
                                                     # i,e ya no contendrá conteos crudos, sino que cada valor representará la probabilidad estimada de
                                                     # encontrar el sistema en el estado correspondiente a su clave-estd después de una iteración aleatoria.

# Ahora bien, por porpositos de familiaridad y graficacion, pasemos la info del diccionario (cada estado y su frec reltv) a un array:
probability_matrix = np.zeros((NT + 1, NT + 1))      # Pues NA+NB <= NT

for (count_A, count_B), probability in joint_distribution.items():
    probability_matrix[count_A, count_B] = probability       # a la posicion NA,NB en la matriz, le guarda una probabilidad (su prob asociada)

# Establescamos el tamaño de la figura y la cantidad de subplots (1 fila, 2 columnas)
fig, axs = plt.subplots(2, 2, figsize=(15, 10))  # Ajustamos para tres subplots

# Histograma para las células A y B
axs[0, 0].hist(counts_A, bins=30, alpha=0.7, label='Células A', color='blue', density=True)
axs[0, 0].hist(counts_B, bins=30, alpha=0.7, label='Células B', color='red', density=True)
axs[0, 0].set_title('Distribución de Células A y B a lo largo de 1000 Iteraciones')
axs[0, 0].set_xlabel('Número de Células')
axs[0, 0].set_ylabel('Frecuencia Normalizada')
axs[0, 0].legend()  # Invocamos legend en el contexto del eje

# Histograma 2D para A y B juntos
h = axs[0, 1].hist2d(counts_A, counts_B, bins=30, cmap='Greens')  #puede usar cmap='inferno') para comprobar que en efecto son cercano a 0 mas que a 100
fig.colorbar(h[3], ax=axs[0, 1])  # Modificado para usar 'fig' en lugar de 'plt'
axs[0, 1].set_title('Histograma 2D de Células A y B')
axs[0, 1].set_xlabel('Células A')
axs[0, 1].set_ylabel('Células B')

# Graficar la matriz de probabilidad conjunta
norm = Normalize(vmin=probability_matrix.min(), vmax=probability_matrix.max()) # normalización de los datos (i,e  el valor más bajo en la matriz de probabilidad se mapeará al color en el extremo inferior del mapa de colores, y el valor más alto se mapeará al extremo superior. Los valores intermedios se mapearán de manera proporcional entre estos extremos)
mapper = ScalarMappable(norm=norm, cmap='viridis')                         # Configura el objeto ScalarMappable con el mapa de colores y la datos normalizados

for (i, j), prob in np.ndenumerate(probability_matrix):                    # Primer subplot: vista completa de la matriz.
    if prob > 0:                                                           # Solo dibuja los cuadrados con probabilidad mayor que cero.
        color = mapper.to_rgba(prob)
        axs[1, 0].add_patch(plt.Rectangle((j, i), 1, 1, color=color))
axs[1, 0].set_xlim(0, probability_matrix.shape[1])
axs[1, 0].set_ylim(0, probability_matrix.shape[0])
axs[1, 0].set_title('Vista Completa de la Matriz de Probabilidad')
axs[1, 0].set_xlabel('Número de Células Tipo B (NB)')
axs[1, 0].set_ylabel('Número de Células Tipo A (NA)')
axs[1, 0].grid(True)
plt.colorbar(mapper, ax=axs[1, 0], orientation='vertical', aspect=20, pad=0.04) # Añade la barra de colores.

NA_max = np.max(np.where(probability_matrix > 0)[0])
NB_max = np.max(np.where(probability_matrix > 0)[1])
for (i, j), prob in np.ndenumerate(probability_matrix[:NA_max+1, :NB_max+1]):  # Segundo subplot: vista enfocada hasta NA_max y NB_max.
    if prob > 0:
        color = mapper.to_rgba(prob)
        axs[1, 1].add_patch(plt.Rectangle((j, i), 1, 1, color=color))
axs[1, 1].set_xlim(0, NB_max + 1)
axs[1, 1].set_ylim(0, NA_max + 1)
axs[1, 1].set_title('Vista Enfocada de la Matriz de Probabilidad')
axs[1, 1].set_xlabel('Número de Células Tipo B (NB)')
axs[1, 1].set_ylabel('Número de Células Tipo A (NA)')
axs[1, 1].grid(True)
plt.colorbar(mapper, ax=axs[1, 1], orientation='vertical', aspect=20, pad=0.04) # Añade la barra de colores.

fig.suptitle('Distribución de Células A y B - Simulación del Juego de la Vida', fontsize=16)
plt.tight_layout()                                                        # Ajusta automáticamente los parámetros del subplot
plt.show()                                                                # Muestra la gráfica.

# Copia de la matriz de probabilidad original
smoothed_probability_matrix = probability_matrix.copy()

# Agregar un pequeño valor a los elementos dentro del rango observado
alpha = 0.0001  # Valor de suavizado
for i in range(NA_max + 1):
    for j in range(NB_max + 1):
        if smoothed_probability_matrix[i, j] == 0:
            smoothed_probability_matrix[i, j] = alpha

# Normalizar la matriz suavizada para que las probabilidades sumen 1
smoothed_probability_matrix = smoothed_probability_matrix / smoothed_probability_matrix.sum()

#-------------------------------------------Ahora usemos dicha matriz suavizada para realizar la simulacion de Metrópolis-------------------------------------

# Encuentra todos los estados con una probabilidad no nula (de la distrb no suavizada)
non_zero_probability_states = [(i, j) for i in range(NT + 1) for j in range(NT + 1) if probability_matrix[i, j] > 0]

# Seleccionar un estado inicial al azar de estos estados
#current_state = non_zero_probability_states[0]                                  #escogemos el primer estado en la matriz con prob no nula
#current_state = random.choice(non_zero_probability_states)                      #escogemos de manera aleatoria un estado en la matriz que tenga prob no nula
indice_max = np.unravel_index(np.argmax(probability_matrix), probability_matrix.shape) #indice asociado a la prob de mayor valor, en la matriz de prob
current_state = indice_max                                                       #escogemos como estado incial, al estado con mayor prob

# Lista para almacenar los estados aceptados
accepted_states = []

# Número de iteraciones para el algoritmo de Metrópolis
num_iterations = 10000

for _ in range(num_iterations):
    # Generar un nuevo estado candidato.
    new_state = (current_state[0] + np.random.choice([-2, 2]), current_state[1] + np.random.choice([-2, 2]))
    #new_state = (current_state[0] + np.random.choice([-10, 10]), current_state[1] + np.random.choice([-10, 10]))
    #new_state = (random.randint(0, NA_max), random.randint(0, NB_max))      # NA_max NB_max. el algoritmo de Metrópolis-Hastings clásico, que normalmente
                                                                             # propone pequeños cambios en el estado actual. Esto podría estar bien si tu
                                                                             # objetivo es muestrear de manera uniforme dentro de un espacio definido, pero
                                                                             # no captura la idea de hacer una "caminata aleatoria" que es característica
                                                                             # del algoritmo de Metrópolis-Hastings (algoritmos de Monte Carlo basados en
                                                                             # cadenas de Markov AKA MCMC).

    # Asegúrate de que el nuevo estado esté dentro de los límites
    if 0 <= new_state[0] < NT and 0 <= new_state[1] < NT:
        # Calcular la razón de aceptación
        current_prob = smoothed_probability_matrix[current_state]
        new_prob = smoothed_probability_matrix[new_state]
        acceptance_ratio = new_prob / current_prob if current_prob > 0 else 0

        # Decidir si se acepta el nuevo estado
        if acceptance_ratio >= 1 or acceptance_ratio > np.random.rand():
            current_state = new_state
            accepted_states.append(current_state)
        else:
            # Si se rechaza, añadir el estado actual nuevamente
            accepted_states.append(current_state)
    else:
        # Si el estado propuesto está fuera de los límites, simplemente añade el estado actual
        accepted_states.append(current_state)

# Después de todas las iteraciones, accepted_states tendremos los estados muestreados via montecarlo. Antes de continuar, verifiquemos que no nos hayamos
#estancado en un solo valor, para ello, al igual que antes, convirtamos la lista en un conjunto para eliminar duplicados y ver qué estados únicos hay
unique_states = set(accepted_states)

if len(unique_states) > 1:                                       # Verificar si hay más de un estado único
    print(f"Hay variabilidad en los estados: {unique_states}")
else:
    print(f"Todos los estados son iguales: {unique_states.pop()}")

# Realizada la anterior verificacion, creemos y grafiquemos la matriz de probabilidad, pero de montecarlo (los datos obtenidos via montecarlo):
NA_values_m, NB_values_m = zip(*accepted_states) # Extraer los valores de NA y NB de los estados aceptados